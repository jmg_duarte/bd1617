alter session set current_schema = candidaturas;

select NOME, idcandidato
from candidatos
where nome like 'PEDRO M.% F.'
;

select *
from candidatos, escolas
where candidatos.ESCOLA = escolas.ESCOLA;

select nome, nomeEscola, descrConcelho
from candidatos inner join escolas using (escola, distrito, concelho)
                inner join concelhos using (distrito, concelho);
              
select ordem, nomecurso, nomeestab
from candidatos 
inner join candidaturas using (idCandidato)
inner join ofertas using (estab, curso)
inner join cursos using (curso)
inner join estabelecimentosSup using (estab)
where IDCANDIDATO = '115349'
order by ORDEM asc;

select idcandidato, nome, nomeexame, nota
from candidatos
inner join notasexames using (idcandidato)
inner join exames using (exame)
where idcandidato = '116280';

select nome, nomeestab
from candidatos
inner join colocacoes using (idCandidato)
inner join ESTABELECIMENTOSSUP using (estab)
where NOME like 'AFONSO %';